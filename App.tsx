import React from 'react';
//import { StyleSheet, Text, View } from 'react-native';
import {Landing} from './skins/pages/Landing'
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Login } from './skins/pages/login';
import {Sanitization} from './skins/pages/sanitization';
import {Patient} from './skins/pages/PatientInformation';
import { Completed } from './skins/pages/completed';
import Tabs from './skins/pages/Tabs';
import { Provider } from 'react-redux';
import { store } from './skins/redux/store';
export default function App() {
  const Stack = createStackNavigator();
  return (
    <Provider store={store}>
    <NavigationContainer>
          <Stack.Navigator headerMode="none">
            <Stack.Screen name="Landing" component={Landing} />
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Tabs" component={Tabs} />
            <Stack.Screen name="Sanitization" component={Sanitization} />
            <Stack.Screen name="Completed" component={Completed} />

            <Stack.Screen name="PatientInformation" component={Patient} />
             {/*<Stack.Screen name="Home" component={Tabs} />
            <Stack.Screen name="Profile" component={Profile} /> */}
          </Stack.Navigator>
        </NavigationContainer>
        </Provider>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
