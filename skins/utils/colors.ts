export const AppColors = {
  white: '#ffffff',
  black: '#000000',
  skyblue: '#3acffc',
  fadedBlack: 'rgba(0,0,0,0.19)',
  grey: 'grey',
  lightGrey: '#bbbbbb',
};

export const StatusColors = {
  requested: '#a7ceaa',
  initiated: '#d0ba61',
  approved: '#30bcbf',
  completed: '#5cc964',
  rejected: '#d56762',
  default: 'grey',
};

export const getStatusColor = (status: string) => {
  switch (status) {
    case 'requested':
      return StatusColors.requested;
      break;
    case 'initiated':
      return StatusColors.initiated;
      break;
    case 'approved':
      return StatusColors.approved;
      break;
    case 'completed':
      return StatusColors.completed;
      break;
    case 'rejected':
      return StatusColors.rejected;
      break;
    default:
      return StatusColors.default;
      break;
  }
}
