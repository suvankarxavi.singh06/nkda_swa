import axios, {AxiosRequestConfig} from 'axios';

export const requestOTP = async (mobile_number: string) => {
    const data = JSON.stringify({mob_no: mobile_number});

    const config: AxiosRequestConfig = {
      method: 'post',
      url: 'http://35.200.255.103:8080/sanitization_worker/getOtp',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };;
  const result = await axios(config)
    .then(res => ({...res.data, success: true}))
    .catch(e => ({error: e.message, success: false}));
  console.log(result);
    return result;
};
