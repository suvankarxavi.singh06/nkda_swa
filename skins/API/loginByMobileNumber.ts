import axios, { AxiosRequestConfig } from 'axios'

export const loginByMobileNumber = async (mobile: string = '8116285848', otp: string = '123456') => {
  const data = JSON.stringify({mob_no: mobile, otp: otp});
  console.log(data)
  const config: AxiosRequestConfig = {
    method: 'post',
    url: 'http://35.200.255.103:8080/sanitization_worker/login',
    headers: {
      'Content-Type': 'application/json',
      // 'authorization': 'key'
    },
    data: data,
  };
    const result = await axios(config)
      .then(res => ({users: res.data, success: true}))
      .catch(e => ({error: e, users: [], success: false}));
    console.log(result);
    return result;
};