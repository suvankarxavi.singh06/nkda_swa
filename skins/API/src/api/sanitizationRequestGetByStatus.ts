import axios, { AxiosRequestConfig } from "axios";

type RequestTypes =
  | "Requested"
  | "Initiated"
  | "Approved"
  | "Completed"
  | "Rejected";

  export interface ISinglePatientFormData {
    mob_no: string;
    email: string;
    name: string;
    age: number | null;
    sex: string;
    address: string;
    action_area: string;
    building: string;
    diabetes_report: string;
    flat_no: string;
    plot_no: string;
    street_no: string;
    landmark: string;
    // is_active: boolean;
    sample_collection_date: string;
    place_of_admition: string;
    status_of_patient: string;
    isolation_status: string;
    group_id: null;
    status: string;
    direct_contuct: string;
    no_of_direct_contact: number;
    result_of_direct_contact: string;
    status_of_direct_contact: string;
    is_vaccinated: boolean;
    reporting_covid_date: string;
    indirect_contact: null;
    hospital_name: string;
    hospital_admition_date: string;
  }
  

export const SanitizationRequestGetByStatus = async (
  type: RequestTypes,
  key: string
) => {
  var config: AxiosRequestConfig = {
    method: "get",
    url:
      "http://35.200.255.103:8080/caller/sanitizationRequestGetByStatus/" +
      type,
    headers: {
      authorization: key,
    },
  };
  const result = await axios(config)
    .then((res) => ({ data: res.data, success: true, error: "" }))
    .catch((e) => ({ error: e, success: false, data: undefined }));
  return result;
};
