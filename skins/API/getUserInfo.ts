import axios, {AxiosRequestConfig} from 'axios';

export const getUserInfo = async (key: string) => {
  var config: AxiosRequestConfig = {
    method: 'get',
    url: 'http://35.200.255.103:8080/sanitization_worker/login',
    headers: {
      authorization: key,
    },
  };
  const result = await axios(config)
    .then(res => ({...res.data[0], success: true}))
    .catch(e => ({error: e, success: false}));
  console.log(result);
  return result;
};
