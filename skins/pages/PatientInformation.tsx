import React, {Component, useState} from 'react';  
import { StyleSheet, View, Text, TextInput, Dimensions, TouchableOpacity, Image, Modal, Alert, ToastAndroid,Button } from 'react-native'; 
import { AppColors, StatusColors, ScreenWidth, ScreenHeight } from '../utils';
import ImagePicker, { ImageOrVideo } from 'react-native-image-crop-picker';
import { useNavigation } from '@react-navigation/native';
export const Patient = () =>  { 
    const defaultSelfie = require('../resource/image/plus.png') 
    const [Selfie, setSelfie] = useState <ImageOrVideo>();
    const updateSelfie = () => {
      ImagePicker.openCamera({
        width: 600,
        height: 600,
        cropping: true,
      }).then(image => {
        console.log(image);
        setSelfie(image);
      }).catch((error) => {
        console.log({error})
      });
    }
    const Submit = () =>
    Alert.alert(
      "Alert",
      "Submited Successfully",
      [
        
        { text: "OK", onPress: (Close)  }
      ]
    );
    const nav = useNavigation();
  const Close =() => {
    nav.navigate('Tabs');
  }
    return (  
        <>
        <View
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'stretch',
          justifyContent: 'space-between',
          paddingVertical: 20,
        }}>
            <View style={{display:'flex',flexDirection:'row'}}>
            <Button title='Close' onPress={Close} /></View>
        <View
          style={{
            marginTop: 80,
            marginBottom: 0,
            paddingVertical: 20,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'center',
            elevation: 2,
            backgroundColor: AppColors.white,
          }}>
         
          <Text style={style.welcome}>Patient Details</Text>
          <Text style={style.details}>
                Name : Mr. Arup Roy
               
            </Text>
            <Text style={style.details}>
                Phone Number : 9239867527
               
            </Text>
            <Text style={style.details}>
                Address : Newtown , Sector -V
               
            </Text>
           
            <Text  style={style.text}>Click Images</Text>
            <View style={style.align}>
               
            <TouchableOpacity activeOpacity={0.7} onPress={(e) => updateSelfie()}>
              <Image  style={style.selfie} source={!!Selfie?{uri: Selfie.path}:defaultSelfie}></Image>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={(e) => updateSelfie()}>
              <Image style={style.selfie} source={!!Selfie?{uri: Selfie.path}:defaultSelfie}></Image>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={(e) => updateSelfie()}>
              <Image style={style.selfie} source={!!Selfie?{uri: Selfie.path}:defaultSelfie}></Image>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={(e) => updateSelfie()}>
              <Image style={style.selfie} source={!!Selfie?{uri: Selfie.path}:defaultSelfie}></Image>
            </TouchableOpacity>
            </View>
<View style={{marginLeft:165, marginTop:150,display:'flex',flexDirection:'row',}}>
            <Button title='Submit' onPress={Submit} />
            
            </View>
        </View>
       

        </View>
        </>
    );  
   
}  
const style = StyleSheet.create({
  labels: {
    //fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 18,
    lineHeight: 22,
    textAlign: 'left',
    margin: 10,
    marginHorizontal: 20,
    color: AppColors.black,
    // textShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
  },
  answerBody: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    paddingHorizontal: 20,
    marginTop: 5,
  },

  welcome: {
    marginVertical: 10,
    //fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 25,
    lineHeight: 30,
    
    color: AppColors.skyblue,
    marginHorizontal: 20,
    paddingLeft: 20,
    textAlign:'left',
    marginLeft:80
    
  },
  details: {
    marginVertical: 10,
    //fontFamily: 'Montserrat',
    fontStyle: 'normal',
    //fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 20,
    
    color: AppColors.black,
    marginHorizontal: 20,
    paddingLeft: 20,
    textAlign:'left',
    marginLeft:10,
    display:'flex',
    flexDirection:'row'
  },
  selfie: {
    borderRadius: 50,
    height: 100,
    width: 90,
    marginTop:70,
    alignItems:'center',
    marginLeft:5
   
},
align:{
    display:'flex',
    flexDirection:'row'
},
text:{
    fontWeight:'bold',
    marginTop:40,
    marginLeft:140,
    color: AppColors.black,
    fontSize:18
}

})