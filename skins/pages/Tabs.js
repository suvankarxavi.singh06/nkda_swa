import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { Completed } from './completed';
import { Sanitization } from './sanitization';
import React from 'react'
import { useSelector } from 'react-redux';
const Tab = createMaterialTopTabNavigator();

function Tabs() {
  //const user = useSelector((state) => state.user)
  //const [accessToken, setAccessToken] = useState(true)
  const accessToken  =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzYW5pdGl6YXRpb25fd29ya2VyX2lkIjoxLCJzYW5pdGl6YXRpb25fd29ya2VyX25hbWUiOiJCaXN3YXJ1cCBEYXMiLCJyb2xlIjoic2FuaXRpemF0aW9uX3dvcmtlciIsImlhdCI6MTYyMTM0NTU0OCwiZXhwIjoxNjIxNDMxOTQ4fQ.H4aTXES9onbNau3wcBpCznyZqgHGaet1EU1uFXHiPCQ"


  // console.log(user)
 
  return (
    
    <Tab.Navigator>
      
       
      <Tab.Screen name="Pending" component={Sanitization} />
      <Tab.Screen name="Completed" component={Completed} />
      
      
    </Tab.Navigator>
    
    
  );
  
}
export default Tabs