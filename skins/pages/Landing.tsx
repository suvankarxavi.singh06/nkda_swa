import React from 'react';
import {
  Image,
  Dimensions,
  View,
  ImageBackground,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { AppColors } from '../utils'

export const Landing = () => {
  const nav = useNavigation();
  return (
    <View
      style={{
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        backgroundColor: AppColors.skyblue,
      }}>
      <ImageBackground
        resizeMode="cover"
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-end',
          alignItems: 'stretch',
        }}
        source={require('../resource/image/hand.jpg')}>
        <ImageBackground
          resizeMode="cover"
          style={{
            width: Dimensions.get('window').width,
            height: 220,
            //backgroundColor: AppColors.white,
            justifyContent: 'space-between',
            padding: 10,
            paddingTop: 0,
          }}
          source={require('../resource/image/wave.png')}>
          <Text style={style.text}>
            Clean City , Sanitized City.
          </Text>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={e => nav.navigate('Login')}>
            <Text style={style.login}>Log In</Text>
          </TouchableOpacity>
        </ImageBackground>
      </ImageBackground>
    </View>
  );
};
const style = StyleSheet.create({
  text: {
    //fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 23,
    lineHeight: 28,
    textAlign: 'center',
    color: AppColors.black,

  },
  login: {
    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 23,
    lineHeight: 49,
    textAlign: 'center',
    color: '#FFFFFF',
    borderColor: AppColors.white,
    borderWidth: 1,
  },
});
