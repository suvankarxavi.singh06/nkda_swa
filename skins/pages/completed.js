import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TextInput, Dimensions, TouchableOpacity, Image, Modal, Alert, Button, ToastAndroid, StatusBar, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { AppColors, StatusColors, ScreenWidth, ScreenHeight } from '../utils';
//import SanitizationTable from './SanitizationTable';
//import {PatientInformation} from './PatientInformation'
import { SanitizationRequestGetByStatus } from "../API/src/api/sanitizationRequestGetByStatus";
export const Completed = () => {
  const nav = useNavigation();
  const openModal = () => {
    nav.navigate('PatientInformation');
  }
  const [columns, setColumns] = useState([
    "Name",
    "Address",
    "Phone",
    "Action"
  ])
  const [direction, setDirection] = useState(null)
  const [selectedColumn, setSelectedColumn] = useState(null)
  // const [pets, setPets] = useState([
  //   {
  //     Name: "Charlie",
  //     Block: "FE",
  //     Area: "III",
  //     Flat: 9,
  //     Address: 'click'
  //   },
  //   {
  //     Name: "Max",
  //     Block: "Male",
  //     Area: "Dog",
  //     Flat: 23,
  //     Address: 'click'
  //   },
  //   {
  //     Name: "Lucy",
  //     Block: "Female",
  //     Area: "Cat",
  //     Flat: 5,
  //     Address: 'click'
  //   },
  //   {
  //     Name: "Oscar",
  //     Block: "Male",
  //     Area: "Turtle",
  //     Flat: 13,
  //     Address: 23
  //   },
  //   {
  //     Name: "Daisy",
  //     Block: "Female",
  //     Area: "Bird",
  //     Flat: 1.7,
  //     Address: 3
  //   },
  //   {
  //     Name: "Ruby",
  //     Block: "Female",
  //     Area: "Dog",
  //     Flat: 6,
  //     Address: 3
  //   },
  //   {
  //     Name: "Milo",
  //     Block: "Male",
  //     Area: "Dog",
  //     Flat: 11,
  //     Address: 7
  //   },
  //   {
  //     Name: "Toby",
  //     Block: "Male",
  //     Area: "Dog",
  //     Flat: 34,
  //     Address: 19
  //   },
  //   {
  //     Name: "Lola",
  //     Block: "Female",
  //     Area: "Cat",
  //     Flat: 4,
  //     Address: 3
  //   },
  //   {
  //     Name: "Jack",
  //     Block: "Male",
  //     Area: "Turtle",
  //     Flat: 13,
  //     Address: 23
  //   },
  //   {
  //     Name: "Bailey",
  //     Block: "Female",
  //     Area: "Bird",
  //     Flat: 2,
  //     Address: 4
  //   },
  //   {
  //     Name: "Bella",
  //     Block: "Female",
  //     Area: "Dog",
  //     Flat: 6,
  //     Address: 10
  //   }
  // ])
  const [rowData, setRowData] = useState([]);

  const accessToken  =

    

      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzYW5pdGl6YXRpb25fd29ya2VyX2lkIjoxLCJzYW5pdGl6YXRpb25fd29ya2VyX25hbWUiOiJCaXN3YXJ1cCBEYXMiLCJyb2xlIjoic2FuaXRpemF0aW9uX3dvcmtlciIsImlhdCI6MTYyMTM0NTU0OCwiZXhwIjoxNjIxNDMxOTQ4fQ.H4aTXES9onbNau3wcBpCznyZqgHGaet1EU1uFXHiPCQ"


    console.log(accessToken)

  useEffect(() => {
    SanitizationRequestGetByStatus("Approved", accessToken).then((rowData) =>
      {

        setRowData(rowData.data)
      }  
    );



  }, []);

  const sortTable = (column) => {
    const newDirection = direction === "desc" ? "asc" : "desc"
    const sortedData = _.orderBy(pets, [column], [newDirection])
    setSelectedColumn(column)
    setDirection(newDirection)
    setPets(sortedData)
  }
  const tableHeader = () => (
    <View style={style.tableHeader}>
      {
        columns.map((column, index) => {
          {
            return (
              <TouchableOpacity
                key={index}
                style={style.columnHeader}
                onPress={() => sortTable(column)}>
                <Text style={style.columnHeaderTxt}>{column + " "}
                  {selectedColumn === column && <MaterialCommunityIcons
                    name={direction === "desc" ? "arrow-down-drop-circle" : "arrow-up-drop-circle"}
                  />
                  }
                </Text>
              </TouchableOpacity>
            )
          }
        })
      }
    </View>
  )


  const Close = () => {
    nav.navigate('Landing');
  }













  return (
    <>
      <View
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'stretch',
          justifyContent: 'space-between',
          paddingVertical: 20,
        }}>
        <View style={{ display: 'flex', flexDirection: 'row' }}>
          <Button title='Close' onPress={Close} /></View>
        <View
          style={{
            marginTop: 80,
            marginBottom: 0,
            paddingVertical: 20,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'center',
            elevation: 2,
            backgroundColor: AppColors.white,
          }}>
          {/* <Image
            style={{ width: 50, height: 50, marginLeft: 30 }}
            source={require('../resource/image/icon.png')}></Image>
          <Text style={style.welcome}>Hi, User</Text> */}
          <Text style={style.description}>WellDone !</Text>
          <Text style={style.work}> Works Done Till Now.</Text>

        </View>


        <View style={style.container}>
          <Text style={style.heading}>Completed Sanitizations </Text>
          <FlatList
            data={rowData}
            style={{ width: "100%" }}
            keyExtractor={(item, index) => index + ""}
            ListHeaderComponent={tableHeader}
            stickyHeaderIndices={[0]}
            renderItem={({ item,index}) => {
              return (
                <View style={{ ...style.tableRow, backgroundColor: index % 2 == 1 ? "#F0FBFC" : "white"}} >
                  <Text style={{ ...style.columnRowTxt, fontWeight: "bold" }}>{item.name}</Text>
                  <Text style={style.columnRowTxt}>{item.address}</Text>
                  <Text style={style.columnRowTxt}>{item.mob_no}</Text>
                 
                  <Text style={style.columnRowTxt}
                    onPress={openModal} >Click</Text>
                </View>
              )
            }}
          />
          <StatusBar />
        </View>


      </View>


    </>





  );



}
const style = StyleSheet.create({
  labels: {
    //fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 18,
    lineHeight: 22,
    textAlign: 'left',
    margin: 10,
    marginHorizontal: 20,
    color: AppColors.black,
    // textShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
  },
  welcome: {
    marginVertical: 10,
    //fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 20,
    lineHeight: 20,
    textAlign: 'left',
    color: AppColors.black,
    marginHorizontal: 20,
    paddingLeft: 10,
  },
  description: {
    marginVertical: 10,
    //fontFamily: 'Montserrat',
    fontStyle: 'normal',
    //fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 15,
    textAlign: 'left',
    color: AppColors.black,
    marginHorizontal: 20,
    paddingLeft: 10,
  },
  work: {
    marginVertical: 10,
    //fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 15,
    textAlign: 'left',
    color: AppColors.black,
    marginHorizontal: 20,
    paddingLeft: 10,
  },
  heading: {
    paddingBottom: 5,
    fontWeight: 'bold',
    fontSize: 14
  },


  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30
  },
  tableHeader: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "#37C2D0",
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    height: 50
  },
  tableRow: {
    flexDirection: "row",
    height: 'auto',
    alignItems: "center",
    width:'auto'
  },
  columnHeader: {
    width: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  columnHeaderTxt: {
    color: "white",
    fontWeight: "bold",
  },
  columnRowTxt: {
    width: "25%",
    textAlign: "center",
  }
});

