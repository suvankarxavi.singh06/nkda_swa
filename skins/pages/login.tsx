import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TextInput, Dimensions, TouchableOpacity, Image, Modal, Alert, ToastAndroid } from 'react-native';
import {useNavigation} from '@react-navigation/native';
import { AppColors, StatusColors, ScreenWidth, ScreenHeight } from '../utils';
import { loginByMobileNumber, getUserInfo } from '../API';
import { useAppDispatch, RootState } from '../redux/store';
import { loginByMobile, insertUserInfo, IUserInfo } from '../redux/slices/user';
import { useSelector } from 'react-redux';
//import moment from 'moment';
import { requestOTP } from '../API/requestOTP';

export const Login = () => {
  const [MobileNumber, setMobileNumber] = useState('');
  const [OTP, setOTP] = useState('');
  const [CanRequest, setcanRequest] = useState(-1)
  const [dialogShow, setdialogShow] = useState(false);
  const [HasOTP, setHasOTP] = useState(false);
  const nav = useNavigation();
  const user = useSelector((state: RootState) => state.user)
  const dispatch = useAppDispatch();
  const handleClick = () => {
    dispatch(loginByMobile({mobileNumber:MobileNumber, otp:OTP}));
  }
  const tryToRequestOTP = async (mobile_number: string) => {
    setcanRequest(30)
    await requestOTP(mobile_number)
      .then(f => {
        if (f.success && typeof f.OTP === 'string') {
          setOTP(f.OTP);
          setHasOTP(true);
        } else {
          ToastAndroid.show(f.error as string, ToastAndroid.SHORT);
        }
      })
      .catch(e => ToastAndroid.show(e as string, ToastAndroid.SHORT));
  }
  useEffect(() => {
    console.log({loggedIn: user.loggedIn})
    if (user.loggedIn) {
       nav.navigate('Tabs');
    }
   
  })
console.log(user);
  return (
    <>
      <View
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'stretch',
          justifyContent: 'space-between',
          paddingVertical: 20,
          // backgroundColor:'#fff'
        }}>
        <View
          style={{
            marginTop: 80,
            marginBottom: 0,
            paddingVertical: 20,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'center',
            elevation: 2,
            backgroundColor: AppColors.white,
          }}>
          <Image
            style={{width: 80, height: 80, marginLeft: 30}}
            source={require('../resource/image/nkda.png')}></Image>
          <Text style={style.welcome}>Welcome to NKDA COVID Health App!</Text>
        </View>
        <View style={{marginBottom: 20}}>
          <Text style={style.labels}>Phone</Text>
          <TextInput
            placeholder="Phone Number"
            keyboardType="phone-pad"
            value={MobileNumber}
            onChangeText={setMobileNumber}
            style={style.inputs}></TextInput>
          <Text style={style.smallText}>
            Enter your registered mobile number in NKDA.
          </Text>
          <Text style={style.labels}>OTP (One Time Password)</Text>
          <TextInput placeholder="XXXX" style={style.inputs} value={OTP} onChangeText={setOTP}></TextInput>
          <Text style={style.smallText}>
            A 4 digits OTP will be sent via SMS to continue logging in.
          </Text>
        </View>
        {HasOTP?<TouchableOpacity
          activeOpacity={0.7}
          style={style.loginButton}
          onPress={e => {
            handleClick();
          }}>
          <Text style={style.login}>Log In</Text>
        </TouchableOpacity>
          :
          <TouchableOpacity
          activeOpacity={0.7}
          style={style.loginButton}
          onPress={e => {
            tryToRequestOTP(MobileNumber);
          }}>
          <Text style={style.login}>Request OTP</Text>
        </TouchableOpacity>}

        <TouchableOpacity activeOpacity={0.7}>
          <Text style={style.resendOTP}>Resend OTP</Text>
        </TouchableOpacity>
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={dialogShow}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setdialogShow(false);
        }}>
        <View
          style={{
            backfaceVisibility: 'visible',
            backgroundColor: AppColors.fadedBlack,
            flex: 1,
            display: 'flex',
            alignItems: 'stretch',
            justifyContent: 'center',
            padding: 50,
          }}>
          <View
            style={{
              backgroundColor: AppColors.white,
              paddingHorizontal: 20,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'stretch',
              justifyContent: 'space-between',
              paddingVertical: 20,
            }}>
            <Text
              style={{
                paddingVertical: 10,
                fontSize: 20,
                textAlign: 'center',
                color: AppColors.black,
                marginBottom: 20,
                borderBottomColor: AppColors.black,
                borderBottomWidth: 1,
              }}>
              Select User
            </Text>
            {user.group.length > 0 ? (
              user.group.map(u => (
                <TouchableOpacity
                  key={u.id}
                  onPress={() => {
                    const call = async () => {
                      const res = await getUserInfo(u.accessToken);
                      dispatch(insertUserInfo({...res, accessToken: user.accessToken} as IUserInfo));
                    };
                    call();
                  }}>
                  <Text
                    style={{
                      paddingVertical: 10,
                      fontSize: 20,
                      backgroundColor: StatusColors.requested,
                      borderRadius: 5,
                      textAlign: 'center',
                      color: AppColors.white,
                      marginBottom: 10,
                    }}>
                    {u.name}
                  </Text>
                </TouchableOpacity>
              ))
            ) : (
              <Text
                style={{
                  paddingVertical: 10,
                  fontSize: 20,
                  textAlign: 'center',
                  color: AppColors.grey,
                }}>
                No User Found. Please login again.
              </Text>
            )}
            <TouchableOpacity onPress={() => setdialogShow(false)}>
              <Text
                style={{
                  paddingVertical: 10,
                  fontSize: 20,
                  backgroundColor: StatusColors.rejected,
                  borderRadius: 5,
                  textAlign: 'center',
                  color: AppColors.white,
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </>
  );
}

const style = StyleSheet.create({
  labels: {
    // fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 18,
    lineHeight: 22,
    textAlign: 'left',
    margin: 10,
    marginHorizontal: 20,
    color: AppColors.black,
    // textShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
  },
  welcome: {
    marginVertical: 10,
    // fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 25,
    lineHeight: 30,
    textAlign: 'left',
    color: AppColors.black,
    marginHorizontal: 20,
    paddingLeft: 10,
  },
  inputs: {
    borderWidth: 1,
    borderColor: AppColors.fadedBlack,
    borderRadius: 10,
    marginHorizontal: 20,
    paddingLeft: 10,
  },
  loginButton: {
    backgroundColor: AppColors.skyblue,
    borderWidth: 1,
    borderColor: AppColors.white,
    marginHorizontal: 20,
    borderRadius: 10,
  },
  login: {
    // fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 23,
    lineHeight: 49,
    textAlign: 'center',
    color: AppColors.white,
    borderColor: AppColors.white,
    borderWidth: 1,
  },
  resendOTP: {
    // fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 18,
    lineHeight: 22,
    textAlign: 'center',
    color: AppColors.skyblue,
  },
  smallText: {
    fontSize: 13,
    color: AppColors.grey,
    marginHorizontal: 20,
    paddingLeft: 10,
  },
});
