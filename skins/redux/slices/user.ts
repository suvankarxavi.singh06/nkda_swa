import {
  createSlice,
  PayloadAction,
  createAsyncThunk,
  SerializedError,
} from '@reduxjs/toolkit';
import {loginByMobileNumber, getUserInfo} from '../../API';
import {showDialog} from './ui';

export interface IUserInfoBase {
  id: number;
  name: string;
  accessToken: string;
}
export interface IUserInfo extends IUserInfoBase {
  mob_no: string;
  email: string;

  age: number;
  sex: string;
  address: string;
  action_area: string;
  building: string;
  createdAt: string;
  updatedAt: string;
}

export interface IUserState extends IUserInfo {
  loggedIn: boolean;
  loggingIn: boolean;
  userSelected: boolean;
  group: IUserInfoBase[];
}

const initialUserState: IUserState = {
  id: -1,
  mob_no: '',
  email: '',
  name: '',
  age: -1,
  sex: 'F',
  address: '',
  action_area: '',
  building: '',
  createdAt: '',
  updatedAt: '',
  loggedIn: false,
  loggingIn: false,
  userSelected: false,
  accessToken: '',
  group: [],
};

export const loginByMobile = createAsyncThunk<IUserInfoBase[],{ mobileNumber: string; otp: string}>(
  'user/loginByMobileNumber',
  async({mobileNumber, otp}:{ mobileNumber: string; otp: string}, thunkAPI) => {
    const userArray = await loginByMobileNumber(mobileNumber, otp);
    const response = Object.values(userArray.users) as IUserInfoBase[]
    console.log({response})
    return response as IUserInfoBase[];
  },
);

export const userSlice = createSlice({
  name: 'user',
  initialState: initialUserState as IUserState,
  reducers: {
    insertUserInfo: (state: IUserState, action: PayloadAction<IUserInfo>) => {
      state = { ...state, ...action.payload, userSelected: true, loggedIn: true, loggingIn: false };
      return state;
    },
    removeUserInfo: (state: IUserState, action: PayloadAction<IUserInfo>) => {
      return initialUserState;
    },
  },
  extraReducers: {
    [loginByMobile.fulfilled.type]: (
      state: IUserState,
      action: PayloadAction<IUserInfoBase[]>,
    ) => {
      state.group = action.payload;
      if (action.payload.length > 0) {
        state.loggedIn = true;
        state.loggingIn = false;
      } else {
        state.loggedIn = false;
        state.loggingIn = false;
      }
    },
    [loginByMobile.pending.type]: (
      state: IUserState,
      action: PayloadAction<undefined>,
    ) => {
      state.loggedIn = false;
      state.loggingIn = true;
    },
    [loginByMobile.rejected.type]: (
      state: IUserState,
      action: PayloadAction<
        unknown,
        string,
        {
          arg: string;
          requestId: string;
          rejectedWithValue: boolean;
          requestStatus: 'rejected';
          aborted: boolean;
          condition: boolean;
        },
        SerializedError
      >,
    ) => {
      state.loggedIn = false;
      state.loggingIn = false;
    },
    //   builder.addCase(
    //     loginByMobile.fulfilled,
    //       (state: IUserState, action: PayloadAction<IUserInfo>) => {
    //       state = {...action.payload, loggedIn: true, loggingIn: false};
    //     },
    //   );
    //     builder.addCase(
    //       loginByMobile.pending,
    //   (state: IUserState, action: PayloadAction<undefined>) => {
    //       state.loggedIn = false;
    //       state.loggingIn = true;
    //   },
    //     );
    //     builder.addCase(
    //       loginByMobile.rejected,
    //   (state: IUserState, action: PayloadAction<unknown, string, { arg: string; requestId: string; rejectedWithValue: boolean; requestStatus: "rejected"; aborted: boolean; condition: boolean; }, SerializedError>) => {
    //     state.loggedIn = false;
    //     state.loggingIn = false;
    //   },
    //     );
  },
});

// Action creators are generated for each case reducer function
export const {insertUserInfo, removeUserInfo} = userSlice.actions;
