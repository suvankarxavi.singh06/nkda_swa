import {
  createSlice,
  PayloadAction,
  createAsyncThunk,
  SerializedError,
} from '@reduxjs/toolkit';

export interface IUIState {
    dialogShow: boolean;
}

const initialState: IUIState = {
    dialogShow: false,
}

export const UISlice = createSlice({
    name: 'ui',
    initialState: initialState as IUIState,
    reducers: {
        showDialog: (state: IUIState, action: PayloadAction<boolean>) => {
            state.dialogShow = action.payload;
        }
    }
})

export const { showDialog } = UISlice.actions;
