import {configureStore} from '@reduxjs/toolkit';
import {userSlice} from './slices/user';
import { useDispatch } from 'react-redux';
import { logger } from 'redux-logger';
import { UISlice } from './slices/ui';

export const store = configureStore({
  reducer: {
    user: userSlice.reducer,
    ui: UISlice.reducer
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware()
    //   .prepend(
    //     // correctly typed middlewares can just be used
    //     additionalMiddleware,
    //     // you can also type middlewares manually
    //     untypedMiddleware as Middleware<
    //       (action: Action<'specialAction'>) => number,
    //       RootState
    //     >,
    //   )
      // prepend and concat calls can be chained
      .concat(logger),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>() 